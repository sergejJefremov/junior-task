<?php
	
	$db = new PDO('mysql:host=localhost;dbname=shop_items', 'root', '');
	$db->exec("SET NAMES UTF8");


	$drop_box_val = trim($_POST['categ_drop_box']);
	$items_for_del = $_POST['data'];

	$all_product = $db->prepare("SELECT * FROM items ORDER BY id ASC");

	if($drop_box_val == 'all'){
		$all_product = $db->prepare("SELECT * FROM items ORDER BY id ASC");
	}else if ($drop_box_val == 'dvd'){
		$all_product = $db->prepare("SELECT * FROM items WHERE type='DVD' ORDER BY id ASC");
	}else if ($drop_box_val == 'book'){
		$all_product = $db->prepare("SELECT * FROM items WHERE type='BOOK' ORDER BY id ASC");
	}else if ($drop_box_val == 'furniture'){
		$all_product = $db->prepare("SELECT * FROM items WHERE type='FURNITURE' ORDER BY id ASC");
	}else if ($drop_box_val == 'delete'){

		if(isset($items_for_del)){
			var_dump($items_for_del);
			foreach ($items_for_del as $id) {

				$spl= "DELETE FROM items WHERE id='$id'";
				$db->exec($spl);
				header("Location: index.php");
			}
		}
	}

	$all_product->execute();
	$product_arr = $all_product->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="ru">

<head>

	<meta charset="utf-8">
	<!-- <base href="/"> -->

	<title>OptimizedHTML 4</title>
	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Template Basic Images Start -->
	<meta property="og:image" content="path/to/image.jpg">
	<link rel="icon" href="img/favicon/favicon.ico">
	<link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon-180x180.png">
	<!-- Template Basic Images End -->
	
	<!-- Custom Browsers Color Start -->
	<meta name="theme-color" content="#000">
	<!-- Custom Browsers Color End -->

	<link rel="stylesheet" href="css/main.min.css">

</head>

<body>
	<header class="header-product-list">
		<div class="container">
			<h1>Product List</h1>
			<div class="delete-action-form">
				<form id="product_post_form" method="post">
					<select id="home_select_id" class="action-options" name="categ_drop_box">
						<option value="all">All products</option>
					  <option value="dvd">DVD-disc</option>
					  <option value="book">Book</option>
					  <option value="furniture">Furniture</option>
					  <option value="delete">Mass Delete Action</option>
					</select>
					<input id="ApplyBtn" class="button button-apply" type="submit" value="Apply"><br>
				</form>
			</div>
		</div>
	</header>

	<section class="item-list">	
		<div class="container">
				<div class="all-items grid">	
					<?php
						foreach ($product_arr as $line) {
							echo "<div class='item grid'>";
								echo "<input class='checkDele' type='checkbox' id='$line[id]' name='delete_item'>";
								echo $line[sku]."<br>";
								echo $line[name]."<br>";
								echo $line[price]." $<br>";
								if($line[type] == "DVD"){
									echo "Size: ".$line[prop]." MB";
								}else if($line[type] == "FURNITURE"){
									echo "Weight: ".$line[prop]." KG";
								}else{
									echo "Dimension: ".$line[prop];
								}
								
							echo "</div>";
						}
					?>
				</div>
				<a class="button" href="/product_add.php">Add New Product</a>.
		</div>
	</section>

	<script src="js/scripts.min.js"></script>

</body>
</html>
